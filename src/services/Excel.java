/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import models.AsignacionModel;
import org.apache.poi.ss.usermodel.DataFormatter;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author thiago
 */
public class Excel {
    public ArrayList<AsignacionModel> ListadoAsignacion =new ArrayList<AsignacionModel>();
    public long NroRemision = -1 ;
    public String fecharemision = "";
    
    
    public int __readFileTest(String ruta) throws IOException {
        int response = -1;
        
        try(FileInputStream archivo = new FileInputStream(new File(ruta))) {
            //Leer arvhivo excel
            XSSFWorkbook workbook = new XSSFWorkbook(archivo);
            //Hoja que se va a leer
            XSSFSheet sheet = workbook.getSheetAt(0);
            //filas
            Iterator<Row> rowiIterator = sheet.iterator();
            DataFormatter dataFormatter = new DataFormatter();
            
            Row row;
            //Recorre cada fila
            int aux=0;
            while(rowiIterator.hasNext()) {
                if(aux > 0) {
                    AsignacionModel obj=new AsignacionModel();
                    row = rowiIterator.next();
                                        
                    String codigo = dataFormatter.formatCellValue( (row.getCell(0)) );
                    codigo = codigo.replace(".", "");
                                                            
                    String cantidad = (row.getCell(1) + "").replace(".0", "");
                    
                    int cant = isInteger(cantidad) ? Integer.parseInt(cantidad) : 0;

                    if(!codigo.equals("") && cant >0 && row.getCell(0) != null) {
                        
                        System.out.print("["+ codigo + "]--------[" + cantidad +"] \n");
                        
                        obj.setCodigo(codigo);                
                        obj.setCantidad( cant );
                        ListadoAsignacion.add(obj);
                    }
                }
                aux++;
            }
            if(ListadoAsignacion.size() > 0 ) return 1;
            
        } catch(Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage() + " <<=====================");
        }        
        return response;
    }
    
    public int __readFile(String ruta) throws IOException {
        int response = -1;
        
        try(FileInputStream archivo = new FileInputStream(new File(ruta))) {
            //Leer arvhivo excel
            XSSFWorkbook workbook = new XSSFWorkbook(archivo);
            //Hoja que se va a leer
            XSSFSheet sheet = workbook.getSheetAt(0);
            //filas
            Iterator<Row> rowiIterator = sheet.iterator();
            DataFormatter dataFormatter = new DataFormatter();
            
            Row row;
            //Recorre cada fila
            int aux=0;
            while(rowiIterator.hasNext()) {
                if(aux > 0) {
                    AsignacionModel obj=new AsignacionModel();
                    row = rowiIterator.next();
                                        
                    String codigo = dataFormatter.formatCellValue( (row.getCell(4)) );
                    codigo = codigo.replace(".", "");
                    
                    String ciudad = dataFormatter.formatCellValue( (row.getCell(12)) );
                                        
                    String cantidad = (row.getCell(6) + "").replace(".0", "");
                    
                    System.out.print("["+ codigo + "]--------[" + cantidad +"] "+ dataFormatter.formatCellValue(row.getCell(12)) + "\n");

                    int cant = isInteger(cantidad) ? Integer.parseInt(cantidad) : 0;

                    if(!codigo.equals("") && cant >0 && row.getCell(0) != null) {
                        obj.setCodigo(codigo);                
                        obj.setCiudad(ciudad);
                        obj.setCantidad( cant );
                        ListadoAsignacion.add(obj);
                    }
                }
                aux++;
            }
            if(ListadoAsignacion.size() > 0 ) return 1;
            
        } catch(Exception e) {
            e.printStackTrace();
            System.err.println(e.getMessage() + " <<=====================");
        }        
        return response;
    }
    
    private static boolean isInteger(String cadena) {
        try {
            // Intenta convertir la cadena a un número
            Integer.parseInt(cadena);

            // Verifica si el número es un entero y si no hay parte decimal
            return true;
        } catch (NumberFormatException e) {
            return false; // Si ocurre una excepción, la cadena no es un número entero
        }
    }
}
