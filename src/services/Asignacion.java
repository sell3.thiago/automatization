/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package services;

import java.util.List;
import java.util.ArrayList;
import models.AsignacionModel;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author thiago
 */
public class Asignacion extends Thread {
    
    private WebDriver driver;
    private Long nroRemision;
    private String fecha;
    
    ArrayList<AsignacionModel> Listado;

    // Constructor que acepta un WebDriver como parámetro
    public Asignacion(WebDriver driver, Long remisionNro, String FechaRemision, ArrayList<AsignacionModel> ListadoAsignacion) {
        this.driver = driver;
        this.nroRemision = remisionNro;
        this.fecha = FechaRemision;
        this.Listado = ListadoAsignacion;
    }

    
    public void __crearAsignacion () throws InterruptedException {
        Actions a = new Actions(driver);
            
        // lista inicial
        WebElement element = driver.findElement(By.cssSelector("li.pureCssMenui0:nth-child(1) > a:nth-child(1)"));
        a.moveToElement(element).build().perform();
        
        Thread.sleep(1000);
        
        driver.findElement(By.cssSelector("li.pureCssMenui:nth-child(4) > a:nth-child(1)")).click();            
        
        Thread.sleep(6000);
        
        driver.findElement(By.id("send")).click();
        
        Thread.sleep(1000);
        
        driver.findElement(By.name("num_documento")).sendKeys(""+nroRemision);
        
        driver.findElement(By.name("proveedor")).sendKeys("COMUNICACION CELULAR S A COMCEL S A");
        
        // fecha formato yyyy-mm-dd
        driver.findElement(By.name("fecha_vencimiento")).sendKeys(fecha);
        
        Thread.sleep(500);
        
        Select tipoIngreso = new Select(driver.findElement(By.name("idtipo_ingreso")));
        tipoIngreso.selectByValue("31");
        
        Thread.sleep(500);
        
        Select tipoProveedor = new Select(driver.findElement(By.name("idtipo_proveedor")));
        tipoProveedor.selectByValue("133");
        driver.findElement(By.className("boton1")).click();
        
        Thread.sleep(400);
            
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();
        alert.accept();
    
        ArrayList<WebElement> elementos = (ArrayList<WebElement>) driver.findElements(By.cssSelector(".rojo"));
        boolean isExist = elementos.size() > 0;
        
        
        if(isExist) {
            Thread.sleep(2000);
            __actualizarAsignacion(driver, nroRemision, Listado);
        }
    }    
    
    public void __actualizarAsignacion(WebDriver driver, Long nroRemision, ArrayList<AsignacionModel> Lista) throws InterruptedException {
        try {
            // Damos clic en regresar
            driver.findElement(By.id("send")).click();


            Thread.sleep(5000);
            driver.findElement(By.cssSelector("body > table:nth-child(24) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(4) > a:nth-child(1) > div:nth-child(1)")).click();

            Thread.sleep(2000);
            
            // Ajusta el tiempo de espera según sea necesario
            WebDriverWait wait = new WebDriverWait(driver, 10);
            
            
            WebElement tabla = driver.findElement(By.cssSelector(".borde"));
            

            // Espera a que la tabla esté presente en el DOM
            wait.until(ExpectedConditions.visibilityOf(tabla));
            
            
            List<WebElement> filas = new ArrayList<>(tabla.findElement(By.tagName("tbody")).findElements(By.tagName("tr")));
            Boolean isExist = false;
            for (WebElement fila : filas) {
                List<WebElement> celdas = fila.findElements(By.tagName("td"));
                // Verifica que haya al menos dos celdas antes de intentar acceder a la segunda celda
                if (celdas.size() >= 2) {
                    // Extrae el código de la segunda celda (índice 1)
                    String codigo = celdas.get(1).getText().trim();

                    // Imprime el código para verificar
                    System.err.println("Código: " + codigo);

                    // Compara el código con el que estás buscando
                    if (codigo.equals(nroRemision+"")) {
                        // Realiza la acción deseada, en este caso, hacer clic en el enlace en la primera celda
                        WebElement botonAccion = celdas.get(0).findElement(By.tagName("a"));

                        // Espera a que el enlace sea clickeable antes de hacer clic
                        wait.until(ExpectedConditions.elementToBeClickable(botonAccion)).click();

                        isExist = true;
                        break;  // Puedes salir del bucle si ya encontraste el código
                    }
                }
            }
            
            
            if(isExist) {
                Thread.sleep(2000);
                __onIngresarInformacion(driver, Lista);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
    public void __onIngresarInformacion (WebDriver driver, ArrayList<AsignacionModel> Lista) throws InterruptedException {
        for(AsignacionModel item: Lista) {
            driver.findElement(By.id("filtro_producto")).sendKeys(item.getCodigo());
            driver.findElement(By.name("cantidad")).sendKeys(item.getCantidad()+"");
            
            driver.findElement(By.cssSelector(".boton1")).click();
            
            Alert alert = driver.switchTo().alert();
            alert.accept();
            
            Thread.sleep(2000);
        }
    }
    
    
    
    
    
    @Override
    public void run() {
        try {
            __crearAsignacion();
        } catch (Exception e) {
        }
    }
            
}
